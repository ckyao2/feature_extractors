import argparse
import numpy as np
import cv2
import h5py
import os
from utilities import *

parser = argparse.ArgumentParser(description='Feature extractor ORB')
parser.add_argument('--path', default='./sacre_coeur/', type=str)
parser.add_argument('--name', default='sacre_coeur_orb', type=str)
parser.add_argument('--num_kp', type=int, default=2048)


arg = parser.parse_args()
print(arg)

orb = cv2.ORB_create(nfeatures=arg.num_kp)

kp_dict = {}
desc_dict = {}

for image_name in os.listdir(arg.path):
	im = cv2.imread(arg.path + image_name)
	gray= cv2.cvtColor(im,cv2.COLOR_BGR2GRAY)
	kp, descriptors = orb.detectAndCompute(gray,None)
	#print(kp)
	#print(descriptors)
	keypoints = np.array([k.pt for k in kp])
	kp_dict[image_name] = keypoints
	desc_dict[image_name] = descriptors
 
# check whether the matches make sense or not
img1 = cv2.imread('./sacre_coeur/02085496_6952371977.jpg')
gray1 = cv2.cvtColor(img1, cv2.COLOR_BGR2GRAY)
img2 = cv2.imread('./sacre_coeur/02928139_3448003521.jpg')
gray2 = cv2.cvtColor(img2,cv2.COLOR_BGR2GRAY)
kp1, desc1 = orb.detectAndCompute(gray1,None)
kp2, desc2 = orb.detectAndCompute(gray2,None)

bf = cv2.BFMatcher(cv2.NORM_HAMMING, crossCheck=False)
matches = bf.knnMatch(desc1, desc2, k=2)

good = []
for m,n in matches:
    if m.distance < 0.8*n.distance:
        good.append([m])
        

img = cv2.drawMatchesKnn(img1,kp1,img2,kp2,good,None,flags=2)
cv2.imwrite("orb_80_matches.png", img)	

save_h5(kp_dict, 'keypoints_'+arg.name+'.h5')
save_h5(desc_dict, 'descriptors_'+arg.name+'.h5')


'''
k2 = load_h5('keypoints_orb.h5')
d2 = load_h5("descriptors_orb.h5")

keypoints = k2
descriptors = d2
'''	
