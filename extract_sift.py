import argparse
import numpy as np
import cv2
import h5py
import os
from utilities import *

parser = argparse.ArgumentParser(description='Feature extractor SIFT')
parser.add_argument('--path', default='./sacre_coeur/', type=str)
parser.add_argument('--name', default='sacre_coeur_sift', type=str)
parser.add_argument('--num_kp', type=int, default=2048)

arg = parser.parse_args()
print(arg)


## maximum number of detected features
num_first_detect = 100000
num_kp = arg.num_kp

sift = cv2.SIFT_create(num_first_detect, contrastThreshold=-10000, edgeThreshold=-10000)

kp_dict = {}
desc_dict = {}

for image_name in os.listdir(arg.path):
  im = cv2.imread(arg.path + image_name)
  gray= cv2.cvtColor(im,cv2.COLOR_BGR2GRAY)
  kp, descriptors = sift.detectAndCompute(gray,None)
  #order keypoints with respect to its score
  #print(kp)
  #print(descriptors)
  top_resps = np.array([x.response for x in kp])
  idxs = np.argsort(top_resps)[::-1]
  keypoints = np.array([k.pt for k in kp])[idxs[:min(len(kp), num_kp)]]
  kp_dict[image_name] = keypoints
  desc_dict[image_name] = descriptors[idxs[:min(len(kp), num_kp)]]
	

save_h5(kp_dict, 'keypoints_'+arg.name+'.h5')
save_h5(desc_dict, 'descriptors_'+arg.name+'.h5')

'''
k2 = load_h5('keypoints_sift.h5')
d2 = load_h5("descriptors_sift.h5")
'''
