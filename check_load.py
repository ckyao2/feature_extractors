import numpy as np
import h5py
import cv2
from utilities import *

kp_sift = load_h5('keypoints_sacre_coeur_sift.h5')
desc_sift = load_h5('descriptors_sacre_coeur_sift.h5')
matches_sift = load_h5('sacre_coeur_sift_2048_ratio_0.7.h5')

all_image_names = list(kp_sift.keys())
print(all_image_names[0])
print(kp_sift[all_image_names[0]])
print(kp_sift[all_image_names[0]].shape)


print(desc_sift[all_image_names[0]])
print(desc_sift[all_image_names[0]].shape)


all_matches_names = list(matches_sift.keys())
print(len(all_matches_names))
print(all_matches_names[0])
print(matches_sift[all_matches_names[0]])
print(matches_sift[all_matches_names[0]].shape)