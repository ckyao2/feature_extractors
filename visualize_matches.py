import argparse
import numpy as np
import cv2
import h5py
import os
from utilities import *
import matplotlib.pyplot as plt


def plot_inlier_matches(ax, img1, img2, inliers):
    """
    Plot the matches between two images according to the matched keypoints
    :param ax: plot handle
    :param img1: left image
    :param img2: right image
    :inliers: x,y in the first image and x,y in the second image (Nx4)
    """
    img2_ax = img2.shape[0]
    img1_re = np.zeros((img2_ax,img1.shape[1]), dtype=img1.dtype)
    img1_re[:img1.shape[0], :] = img1
    
    res = np.hstack([img1_re, img2])
    ax.set_aspect('equal')
    ax.imshow(res, cmap='gray')
    
    ax.plot(inliers[:,0], inliers[:,1], '+r')
    ax.plot(inliers[:,2] + img1_re.shape[1], inliers[:,3], '+r')
    ax.plot([inliers[:,0], inliers[:,2] + img1_re.shape[1]],
            [inliers[:,1], inliers[:,3]], 'r', linewidth=0.4)
    ax.axis('off')
    
'''
# check whether the matches make sense or not
img1 = cv2.imread('./sacre_coeur/02085496_6952371977.jpg')
gray1 = cv2.cvtColor(img1, cv2.COLOR_BGR2GRAY)
img2 = cv2.imread('./sacre_coeur/02928139_3448003521.jpg')
gray2 = cv2.cvtColor(img2,cv2.COLOR_BGR2GRAY)
'''

img1 = cv2.imread('./sacre_coeur/49118808_3956125368.jpg')
gray1 = cv2.cvtColor(img1, cv2.COLOR_BGR2GRAY)
img2 = cv2.imread('./sacre_coeur/67090050_2810319680.jpg')
gray2 = cv2.cvtColor(img2,cv2.COLOR_BGR2GRAY)

kps = load_h5('./keypoints_sacre_coeur_r2d2.h5')

'''
kp1 = kps['02085496_6952371977.jpg']
kp2 = kps['02928139_3448003521.jpg']
'''

kp1 = kps['49118808_3956125368.jpg']
kp2 = kps['67090050_2810319680.jpg']

matches = load_h5('./sacre_coeur_r2d2_2048_ratio_0.85.h5')
good = matches['49118808_3956125368.jpg_67090050_2810319680.jpg']

print(good.shape)
print(kp1.shape)
print(kp1[good[:,0],:].shape)
print(good[:,0])


'''
inliers = []
inliers.append(kp1[good[:,0],:])
inliers.append(kp2[good[:,1],:])
'''
computed_inliers = np.hstack([kp1[good[:,0],:], kp2[good[:,1],:]])
#computed_inliers = np.hstack([kp1[good[:,0],:], kp2[good[:,1],:]])
print(computed_inliers.shape)

fig, ax = plt.subplots(figsize=(20,10))
plot_inlier_matches(ax, gray1, gray2, computed_inliers)
plt.savefig('r2d2_85_matches.png')



'''
img = cv2.drawMatchesKnn(img1,kp1,img2,kp2,good,None,flags=2)
cv2.imwrite("sift_80_matches.png", img)	
'''
'''
save_h5(kp_dict, 'keypoints_'+arg.name+'.h5')
save_h5(desc_dict, 'descriptors_'+arg.name+'.h5')
'''