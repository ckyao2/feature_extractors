import h5py

def save_h5(dict_to_save, filename):
	with h5py.File(filename, 'w') as f:
		for key in dict_to_save:
			f.create_dataset(key, data=dict_to_save[key])
			
def load_h5(filename):
	dictionary = {}
	with h5py.File(filename, 'r') as f:
		keys = [key for key in f.keys()]
		for key in keys:
			dictionary[key] = f[key][()]
	return dictionary
