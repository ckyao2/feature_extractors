import argparse
import numpy as np
import cv2
import h5py
import os
from utilities import *



def compute_matches(desc1, desc2, kps1=None, kps2=None):
    # to-do
    # norm parameter will be added
    #bf = cv2.BFMatcher(cv2.NORM_L1, crossCheck=False)
    bf = cv2.BFMatcher(cv2.NORM_L2, crossCheck=False)
    #matches = bf.match(desc1, desc2)
    #bf = cv2.BFMatcher(cv2.NORM_HAMMING, crossCheck=False)
    matches = bf.knnMatch(desc1, desc2, k=2)
    return matches
    
if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Brute force feature matcher with ratio test')
    #parser.add_argument('--path', default='./dataset', type=str)
    parser.add_argument('--desc', type=str)
    parser.add_argument('--save_name', type=str)
    parser.add_argument('--ratio', type=float, default=0.7)
    
    #implement brute force matching with the ratio test

    arg = parser.parse_args()
    print(arg) 
    
    descriptors = load_h5(arg.desc)
    all_image_names = list(descriptors.keys())
    print(all_image_names[:5])
    
      
    match_dictionary = {}
    
    for i in range(0,len(all_image_names)):
      for j in range(i+1, len(all_image_names)):
        matches = compute_matches(descriptors[all_image_names[i]] , descriptors[all_image_names[j]])
        if i==0 and j==1:
          print(descriptors[all_image_names[i]].shape)
          print(descriptors[all_image_names[j]].shape)
        match_dictionary["{}_{}".format(all_image_names[i], all_image_names[j])] = [[m.queryIdx, m.trainIdx] for m, n in matches if m.distance < arg.ratio*n.distance]
        #match_dictionary["{}_{}".format(all_image_names[i], all_image_names[j])] = [[m.queryIdx, m.trainIdx] for m in matches]
        if i==0 and j==1:
          print(len(match_dictionary["{}_{}".format(all_image_names[i], all_image_names[j])]))

    		
    #print(match_dictionary)
    save_h5(match_dictionary, arg.save_name+'_ratio_'+str(arg.ratio)+'.h5')
    #save_h5(match_dictionary, arg.save_name+'_cross_check'+'.h5')
    	
    
    
